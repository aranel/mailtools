"""
Utility script for mailing list management using iRedMail with the PostgreSQL
backend.

Requires: Python 2.7+, psycopg2
Tested on: Python 2.7.6, PostgreSQL 9.3, psycopg2 2.6

Copyright 2015 Eugene Wee

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import absolute_import, print_function, unicode_literals

import argparse
import contextlib
import psycopg2
import re
import sys

from settings import DATABASE

# Email address regex for detecting obvious typo errors:
EMAIL_RE = re.compile(r'^\S+@[\w-]+\.[\w-]{2,}$', re.UNICODE)


def force_text(s, encoding='utf-8'):
    """
    Returns a unicode text object representing s.
    Treats bytestrings using the encoding codec.
    """
    try:
        return unicode(s)
    except UnicodeDecodeError:
        return str(s).decode(encoding)


@contextlib.contextmanager
def commit_on_success(conn):
    """
    Context manager to commit a transaction given the database connection when
    control leaves the with statement block successfully.
    """
    yield
    conn.commit()


@contextlib.contextmanager
def get_cursor(conn):
    """
    Context manager to return a cursor given the database connection, and close
    the cursor when control leaves the with statement block.
    """
    try:
        cursor = conn.cursor()
        yield cursor
    finally:
        cursor.close()


class MailingList(object):
    """
    Models a mailing list and the operations to be managed.
    """
    def __init__(self, dbconn, email, verbose=False):
        self._dbconn = dbconn
        self.email = email
        self._verbose = verbose

    @property
    def domain(self):
        """
        Returns the domain portion of the mailing list email address.
        """
        return self.email[self.email.find('@') + 1:]

    def print_info(self):
        """
        Prints basic info concerning the mailing list.
        """
        conn = self._dbconn
        with commit_on_success(conn), get_cursor(conn) as cursor:
            cursor.execute(
                """SELECT "goto", "created", "modified", "active" FROM "alias"
                   WHERE "address"=%(address)s;""",
                {'address': self.email},
            )
            row = cursor.fetchone()
            if row:
                member_emails = force_text(row[0]).split(',')
                created_datetime = row[1]
                modified_datetime = row[2]
                active = row[3] == 1
                list_exists = True
            else:
                list_exists = False

        if list_exists:
            print('Mailing List: {}'.format(self.email))
            print('Member count: {}'.format(len(member_emails)))
            print('Active: {}'.format('yes' if active else 'no'))
            if self._verbose:
                print('Created: {}'.format(created_datetime))
                print('Modified: {}'.format(modified_datetime))
        else:
            print('Mailing list <{}> does not exist.'.format(self.email))

    def print_members(self):
        """
        Prints the email addresses of the members of the mailing list, sorted.
        """
        with commit_on_success(self._dbconn):
            member_emails, list_exists = self._get_members()

        if list_exists:
            if member_emails:
                for email in sorted(member_emails):
                    print(email)

                if self._verbose:
                    print('{} members.'.format(len(member_emails)))
            elif self._verbose:
                print('Mailing list <{}> is empty.'.format(self.email))
        elif self._verbose:
            print('Mailing list <{}> does not exist.'.format(self.email))

    def add_members(self, emails):
        """
        Adds the email addresses provided to the mailing list.
        """
        with commit_on_success(self._dbconn):
            member_emails, list_exists = self._get_members()
            goto_field = ','.join(member_emails.union(emails))
            with get_cursor(self._dbconn) as cursor:
                if list_exists:
                    cursor.execute(
                        """UPDATE "alias" SET "goto"=%(goto)s, "modified"=now()
                           WHERE "address"=%(address)s;""",
                        {'goto': goto_field, 'address': self.email},
                    )
                else:
                    cursor.execute(
                        """INSERT INTO "alias"
                           ("address", "goto", "domain", "islist", "created",
                            "modified")
                           VALUES (%(address)s, %(goto)s, %(domain)s, 1, now(),
                                   now());""",
                        {'address': self.email,
                         'goto': goto_field,
                         'domain': self.domain},
                    )

        if self._verbose:
            members_added = emails.difference(member_emails)
            if members_added:
                print('These members were added to <{}>:'.format(self.email))
                for email in members_added:
                    print(email)

            duplicates = member_emails.intersection(emails)
            if duplicates:
                print('These members were already in <{}>:'.format(self.email))
                for email in duplicates:
                    print(email)

    def delete_members(self, emails):
        """
        Deletes the email addresses provided from the mailing list.
        """
        with commit_on_success(self._dbconn):
            member_emails, list_exists = self._get_members()
            goto_field = ','.join(member_emails.difference(emails))
            if list_exists:
                with get_cursor(self._dbconn) as cursor:
                    cursor.execute(
                        """UPDATE "alias" SET "goto"=%(goto)s, "modified"=now()
                           WHERE "address"=%(address)s;""",
                        {'goto': goto_field, 'address': self.email},
                    )

        if self._verbose:
            members_deleted = member_emails.intersection(emails)
            if members_deleted:
                print('These members were deleted from '
                      '<{}>:'.format(self.email))
                for email in members_deleted:
                    print(email)

            not_members = emails.difference(member_emails)
            if not_members:
                print('These were not members of <{}>:'.format(self.email))
                for email in not_members:
                    print(email)

    def activate(self):
        """
        Sets the mailing list to active.
        """
        with commit_on_success(self._dbconn):
            active, list_exists = self._get_active()
            if list_exists:
                if not active:
                    with get_cursor(self._dbconn) as cursor:
                        cursor.execute(
                            """UPDATE "alias" SET "active"=1, "modified"=now()
                               WHERE "address"=%(address)s;""",
                            {'address': self.email},
                        )

                    if self._verbose:
                        print('Mailing list <{}> is '
                              'active.'.format(self.email))
                elif self._verbose:
                    print('Mailing list <{}> could not be activated as it is '
                          'already active.'.format(self.email))
            else:
                print('Mailing list <{}> could not be activated as it does '
                      'not exist.'.format(self.email),
                      file=sys.stderr)

    def deactivate(self):
        """
        Sets the mailing list to inactive.
        """
        with commit_on_success(self._dbconn):
            active, list_exists = self._get_active()
            if list_exists:
                if active:
                    with get_cursor(self._dbconn) as cursor:
                        cursor.execute(
                            """UPDATE "alias" SET "active"=0, "modified"=now()
                               WHERE "address"=%(address)s;""",
                            {'address': self.email},
                        )

                    if self._verbose:
                        print('Mailing list <{}> is '
                              'inactive.'.format(self.email))
                elif self._verbose:
                    print('Mailing list <{}> could not be deactivated as it '
                          'is already inactive.'.format(self.email))
            else:
                print('Mailing list <{}> could not be deactivated as it does '
                      'not exist.'.format(self.email),
                      file=sys.stderr)

    def _get_members(self):
        member_emails = []
        with get_cursor(self._dbconn) as cursor:
            cursor.execute(
                'SELECT "goto" FROM "alias" WHERE "address"=%(address)s;',
                {'address': self.email},
            )
            row = cursor.fetchone()
            if row:
                member_emails = force_text(row[0]).split(',')
                list_exists = True
            else:
                list_exists = False
        return (set(member_emails), list_exists)

    def _get_active(self):
        with get_cursor(self._dbconn) as cursor:
            cursor.execute(
                'SELECT "active" FROM "alias" WHERE "address"=%(address)s;',
                {'address': self.email},
            )
            row = cursor.fetchone()
            if row:
                active = row[0] == 1
                list_exists = True
            else:
                active = False
                list_exists = False
        return (active, list_exists)


def create_parser():
    """
    Constructs command line argument parser.
    """
    parser = argparse.ArgumentParser(
        description='Mailing list management using iRedMail with the '
                    'PostgreSQL backend. When called with the mailing list '
                    'argument of "all", prints the existing mailing lists. '
                    'When called with an email address as the mailing list '
                    'argument but without options, prints information about '
                    'the specified mailing list.',
    )
    parser.add_argument(
        'mailing_list',
        help='Email address of mailing list or "all".',
    )
    parser.add_argument(
        '--members',
        action='store_true',
        default=False,
        help='Print the email addresses of the mailing list members.',
    )
    parser.add_argument(
        '--add',
        metavar='emails',
        help='Comma-separated list of email addresses of members to be added '
             'to the mailing list.',
    )
    parser.add_argument(
        '--delete',
        metavar='emails',
        help='Comma-separated list of email addresses of members to be '
             'deleted from the mailing list.',
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--activate',
        action='store_true',
        default=False,
        help='Activate the mailing list.',
    )
    group.add_argument(
        '--deactivate',
        action='store_true',
        default=False,
        help='Deactivate the mailing list.',
    )
    parser.add_argument(
        '--verbose',
        action='store_true',
        default=False,
        help='Enable verbose output.',
    )
    return parser


def print_mailing_lists(verbose=False):
    """
    Prints the existing mailing lists.
    """
    conn = psycopg2.connect(**DATABASE)
    if verbose:
        with commit_on_success(conn), get_cursor(conn) as cursor:
            cursor.execute("""SELECT "address", "goto", "active" FROM "alias"
                              WHERE "islist"=1 ORDER BY "address";""")
            for row in cursor:
                email = force_text(row[0])
                member_emails = force_text(row[1]).split(',')
                active_label = 'active' if row[2] == 1 else 'inactive'
                print('{} ({}; {} members)'.format(email,
                                                   active_label,
                                                   len(member_emails)))
    else:
        with commit_on_success(conn), get_cursor(conn) as cursor:
            cursor.execute("""SELECT "address" FROM "alias"
                              WHERE "islist"=1 ORDER BY "address";""")
            for row in cursor:
                email = force_text(row[0])
                print(email)
    conn.close()


class ValidationError(Exception):
    """
    Exception to be raised for validation errors.
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return unicode(self).encode('utf8')

    def __unicode__(self):
        return self.message


class InvalidEmailAddress(ValidationError):
    """
    Exception to be raised for obviously invalid email addresses.
    """
    def __init__(self, email, message='Invalid email address'):
        super(InvalidEmailAddress, self).__init__(message)
        self.email = email


def clean_email(email):
    """
    Returns cleaned email address, or raises InvalidEmailAddress exception for
    an obviously invalid email address.
    """
    email = email.strip().lower()
    if not EMAIL_RE.match(email):
        raise InvalidEmailAddress(email)
    return email


def clean_emails(emails):
    """
    Returns the cleaned email addresses as a (possibly empty) list, or raises
    InvalidEmailAddress exception for an obviously invalid email address.
    """
    if emails is None:
        return []
    return [clean_email(email) for email in force_text(emails).split(',')
            if email]


def clean_email_args(args):
    """
    Returns a tuple consisting of the cleaned email addresses from the command
    line arguments.
    """
    try:
        mailing_list_email = clean_email(force_text(args.mailing_list))
    except InvalidEmailAddress as e:
        raise ValidationError(
            'Mailing list email address <{}> is invalid.'.format(e.email),
        )

    try:
        add_emails = clean_emails(args.add)
    except InvalidEmailAddress as e:
        raise ValidationError(
            'Email address to be added <{}> is invalid.'.format(e.email),
        )

    try:
        delete_emails = clean_emails(args.delete)
    except InvalidEmailAddress as e:
        raise ValidationError(
            'Email address to be deleted <{}> is invalid.'.format(e.email),
        )

    return (mailing_list_email, set(add_emails), set(delete_emails))


if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()

    if force_text(args.mailing_list) == 'all':
        print_mailing_lists(args.verbose)
    else:
        try:
            (mailing_list_email,
             add_emails,
             delete_emails) = clean_email_args(args)
        except ValidationError as e:
            print(e, file=sys.stderr)
            sys.exit()
        members = args.members
        activate = args.activate
        deactivate = args.deactivate
        verbose = args.verbose

        conn = psycopg2.connect(**DATABASE)
        mailing_list = MailingList(conn, mailing_list_email, verbose=verbose)

        if members:
            mailing_list.print_members()
        elif add_emails or delete_emails or activate or deactivate:
            if add_emails:
                mailing_list.add_members(add_emails)

            if delete_emails:
                mailing_list.delete_members(delete_emails)

            if activate:
                mailing_list.activate()
            elif deactivate:
                mailing_list.deactivate()
        else:
            mailing_list.print_info()
        conn.close()
